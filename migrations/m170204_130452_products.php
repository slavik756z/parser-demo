<?php

use yii\db\Migration;

class m170204_130452_products extends Migration
{
    public function up()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->defaultValue(512),
            'price' => $this->float(),
            'store' => $this->string()
        ]);

        $this->createTable('price_coef', [
            'id' => $this->primaryKey(),
            'coef_value' => $this->float(),
        ]);

        $this->insert('price_coef',['id' => 100,'coef_value' => 1.1]);

    }

    public function down()
    {
        $this->dropTable('products');
        $this->dropTable('price_coef');
    }

}
