$('.send').click(function(){
    $.ajax({
        url: '/parser/get-url?url=' + $('input.url').val(),
        success: function(data) {
            $('.body-content').html(data);
        },
        beforeSend: function () {
            $('.body-content').html('<img src="/images/89.gif" alt=Загрузка..." id="loading" />');
        },
        error: function() {
            $('.body-content').html('Ошибка обращения к серверу');
        }
    });
});

function save_product(){
    var product = {
        title: $('span#title').text(),
        price: $('span#total-price').text(),
        store:$('span#store').text()
    };
    $.ajax({
        type: 'POST',
        url: '/products/create',
        data:{'Products':product},
        success: function(data) {
            $('.body-content').html(data);
        },
        beforeSend: function () {
            $('.body-content').html('<img src="/images/89.gif" alt=Загрузка..." id="loading" />');
        },
        error: function() {
            $('.body-content').html('Ошибка обращения к серверу');
        }
    });
}