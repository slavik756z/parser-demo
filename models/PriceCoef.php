<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "price_coef".
 *
 * @property integer $id
 * @property double $coef_value
 */
class PriceCoef extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price_coef';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coef_value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coef_value' => 'Coef Value',
        ];
    }
}
