<?php

namespace app\models;


use yii\base\ErrorException;
use yii\base\Model;

class ExchangeRate extends Model
{
    /**
     * Get exchange currency rate for this date
     *
     * @return array
     */
    static public function getExchangeRate()
    {

        $uah = null;
        $exchangeArray = array();
        try {
            $cbrCurs = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp?date_req=' . date('d.m.Y')); // TODO
        }catch (ErrorException $e){
            return array();
        }

        $cbrCurs = simplexml_load_string($cbrCurs);

        foreach ($cbrCurs->Valute as $value) {
            if ($value->CharCode == 'UAH') {
                $uah = str_replace(',', '.', $value->Value) / $value->Nominal;
            }
            if ($value->CharCode == 'USD' || $value->CharCode == 'EUR' || $value->CharCode == 'CNY') {
                $exchangeArray[(string)$value->CharCode] = str_replace(',', '.', $value->Value)/$value->Nominal;
            }
        }

        $resultArray = array();

        foreach ($exchangeArray as $key => $value) {
            $resultArray[(string)$key] = round($value / $uah, 3);
        }
        $resultArray['RUB'] = round(1/$uah, 3);

        return $resultArray;
    }
}