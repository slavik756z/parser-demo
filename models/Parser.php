<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Parser extends Model
{
    public $product;

    /**
     * @param $url
     */
    public function parseShop($url)
    {
        if ($this->isUrl($url)) {
            $this->selectShop($url);
        } else {
            $this->addError('url','Это не ссылка');
        }
    }

    /**
     * @param $url
     * @return int
     */
    private function isUrl($url)
    {
        return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
    }

    /**
     * @param $url
     */
    private function selectShop($url)
    {
        $url_array = parse_url($url);
        switch ($url_array['host']){
            case 'www.ebay.com':
                $this->explodeEbay($url);
                break;
            case 'www.aliexpress.com':
                $this->explodeAliExpress($url);
                break;
            case 'ru.aliexpress.com':
                $this->explodeAliExpress($url);
                break;
            case 'world.taobao.com':
                $this->explodeTaobao($url);
                break;
            case 'www.amazon.com':
                $this->explodeAmazon($url);
                break;
            default :
                $this->addError('matches','Нет такого магазина в списке');
                break;
        }
    }

    /**
     * @param $url
     */
    private function explodeEbay($url)
    {
        // - получение страницы
        $data = file_get_contents($url);

        // - обьект обработки
        $document = \phpQuery::newDocument($data);
        $block = $document->find('#CenterPanelInternal');
        $pq = pq($block);

        // - цена
        if($price = $pq->find('span#prcIsum')->text()){
             // TODO если понадобится описать определенную логику
        }else if($price = $pq->find('#mm-saleDscPrc')->text()){
            // TODO если понадобится описать определенную логику
        }else{
            $this->addError('price','Цена не найдена');
        }
        $price = trim($price);
        $price = str_replace(',','',$price);

        // - доставка
        $shipping = $pq->find('#fshippingCost')->text();
        $shipping = trim($shipping);
        if($shipping != '' && $shipping == 'FREE'){
            $shipping = 0;
        }

        // - название
        $pq->find('#itemTitle span')->remove();
        $title = $pq->find('#itemTitle')->text();

        $this->product = array(
            'store' => 'eBay',
            'title' => $title,
            'price' => substr($price,4),
            'shipping' => substr($shipping,1),
            'total_price' => substr($price,4) + substr($shipping,1)
        );
    }

    /**
     * @param $url
     */
    private function explodeAliExpress($url)
    {
        // - настройки заголовков для получения цен в долларах
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=> "cookie: aep_usuc_f=c_tp=USD"
            )
        );

        $context = stream_context_create($opts);

        // - получение страницы
        $data = file_get_contents($url,false,$context);

        // - обьект обработки
        $document = \phpQuery::newDocument($data);
        $block = $document->find('#j-detail-page');
        $pq = pq($block);

        // - цена
        if($price = $pq->find('#j-sku-discount-price')->text()) {
            // TODO если понадобится описать определенную логику
        }else if($price = $pq->find('span#j-sku-price')->text()){
            // TODO если понадобится описать определенную логику
        }else{
            $this->addError('price','Цена не найдена');
        }
        $price = trim($price);
        $price = str_replace(',','',$price);

        // - доставка
        $shipping = $pq->find('.logistics-cost')->text();
        $shipping = trim($shipping);
        if($shipping != '' && $shipping == 'FREE'){
            $shipping = 0;
        }

        // - название
        $title = $pq->find('.product-name')->text();

        $this->product = array(
            'store' => 'AliExpress',
            'title' => $title,
            'price' => $price,
            'shipping' => substr($shipping,1),
            'total_price' => $price + substr($shipping,1)
        );
    }

    /**
     * @param $url
     */
    private function explodeAmazon($url)
    {
        // - получение страницы
        $data = file_get_contents($url);

        // - обьект обработки
        $document = \phpQuery::newDocumentHTML($data);
        $block = $document->find('#centerCol');
        $pq = pq($block);

        // - цена
        if($price = $pq->find('tr > td > span#priceblock_ourprice')->text()){
            // TODO если понадобится описать определенную логику
        }else if($price = $pq->find('span#priceblock_dealprice')->text()){
            // TODO если понадобится описать определенную логику
        }else if($price = $pq->find('span#priceblock_saleprice')->text()){
            // TODO если понадобится описать определенную логику
        }else{
            $this->addError('price','Цена не найдена');
            $this->addError('Amazon','Амазон не всегда получает страницу,попробуйте несколько раз');
        }
        $price = trim($price);
        $price = str_replace(',','',$price);

        // - доставка
        $shipping = $pq->find('#fshippingCost')->text();
        $shipping = trim($shipping);
        if($shipping != '' && $shipping == 'FREE'){
            $shipping = 0;
        }

        // - название
        $title = $pq->find('#productTitle')->text();

        $this->product = array(
            'store' => 'Amazon',
            'title' => $title,
            'price' => substr($price,1),
            'shipping' => substr($shipping,1),
            'total_price' => substr($price,1) + substr($shipping,1)
        );
    }

    /**
     * @param $url
     */
    private function explodeTaobao($url)
    {
        // - получение страницы
        $data = file_get_contents($url);

        // - обьект обработки
        $document = \phpQuery::newDocument($data);
        $pq = pq($document);
        // - цена
        if($price = $pq->find('.tb-rmb-num > span')->eq(0)->text()){
            // TODO если понадобится описать определенную логику
        }else if($price = $pq->find('#mm-saleDscPrc')->text()){
            // TODO если понадобится описать определенную логику
        }else{
            $this->addError('price','Цена не найдена');
        }
        $price = trim($price);
        $price = str_replace(',','',$price);

        // - доставка
        $shipping = $pq->find('#shipping')->text();
        $shipping = trim($shipping);
        if($shipping != '' && $shipping == 'FREE'){
            $shipping = 0;
        }

        // - название
        $title = $pq->find('span.t-title')->text();

        $this->product = array(
            'store' => 'Taobao',
            'title' => $title,
            'price' => $price,
            'shipping' => substr($shipping,1),
            'total_price' => $price + substr($shipping,1)
        );
    }

}