<?php

namespace app\controllers;

use app\models\Parser;
use Yii;
use yii\web\Controller;


class ParserController extends Controller
{


    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetUrl($url)
    {
        $model = new Parser();
        $model->parseShop($url);
        if(!$model->hasErrors()){
            return $this->renderPartial('product_info',['model' => $model]);
        }else{
            return $this->renderPartial('error',['model' => $model]);
        }
    }
}