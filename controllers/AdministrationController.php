<?php

namespace app\controllers;

use app\models\PriceCoef;
use yii\filters\AccessControl;
use Yii;

class AdministrationController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $model = PriceCoef::findOne(100);
        return $this->render('index',['model' => $model]);
    }

    public function actionUpdateCoef()
    {
        $model = PriceCoef::findOne(100);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['/']);
        }else{
            var_dump($model->errors);
        };

    }

}
