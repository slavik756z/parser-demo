<?php

namespace app\components;

use app\models\ExchangeRate;
use yii\base\Widget;

Class ExchangeRateWidget extends Widget{

    public $curs;

    public function init()
    {
        parent::init();
        if ($this->curs === null) { // - Можно ввести свой список для вывода,если нет,то используем с CBR для RUB,USD,EUR
            $this->curs = ExchangeRate::getExchangeRate();
        }
    }

    public function run()
    {
        if (empty($this->curs)){
            $this->curs = array('Ошибка' => 'Нет курса');
        }
        return $this->render('exchange_rate',['curs' => $this->curs]);
    }
}