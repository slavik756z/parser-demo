<div id="product-info" class="col-md-8 col-md-offset-2">
    <h2>Название:<span id="title"><?= $model->product['title']?></span></h2>
    <h4>Цена:<span id="price"><?= $model->product['price'] ?></span></h4>
    <h4>Доставка:<span id="shipping"><?= $model->product['shipping']?></span></h4>
    <h4>Общая стоимость:<span id="total-price"><?= $model->product['total_price'] ?></span></h4>
    <h4>Магазин:<span id="store"><?= $model->product['store']?></></h4>
    <button onclick="save_product()" type="button" class="save-product btn btn-success">Сохранить товар</button>
</div>