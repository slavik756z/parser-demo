<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\ExchangeRateWidget;
use app\models\PriceCoef;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $curs app\models\ExchangeRate */

echo ExchangeRateWidget::widget(['curs' => $curs]);

$this->title = 'Товары';
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'label' =>'price',
                'content' => function($model) use ($curs){
                    $price_coef = PriceCoef::findOne(100);
                    $price = round(($model->price*($price_coef->coef_value/100) + $model->price),2);
                    if ($model->store == 'Taobao'){
                       return $price . 'CNY/'.  round($price*$curs['CNY'],2) . 'UAH';
                    }else{
                        return $price . 'USD/' . round($price*$curs['USD'],2) . 'UAH';
                    }
                }
            ],
            'store'
        ],
    ]); ?>
</div>
