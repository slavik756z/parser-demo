<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
?>
<h1>Админ панель</h1>


<div class="col-md-8 col-md-offset-2">
    <form method="post" class="form-horizontal" action="/administration/update-coef">
        <div class="form-group">
            <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf">
            <label class="col-sm-4 control-label" for="exampleInputName2">Введите процент наценки</label>
            <div class="col-sm-6">
                <input type="text" name='PriceCoef[coef_value]' value="<?= $model->coef_value ?>" class="url form-control" id="exampleInputName2" placeholder="1.2">
            </div>
            <button type="submit" class="send btn btn-success col-sm-2">Отправить</button>
        </div>
    </form>
</div>

